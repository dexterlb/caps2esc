#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <linux/input.h>

typedef enum {
    no_flags     = 0,
    help_flag    = 1,
} flags_t;

// clang-format off
const struct input_event
space_up         = {.type = EV_KEY, .code = KEY_SPACE,    .value = 0},
super_up         = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 0},
space_down       = {.type = EV_KEY, .code = KEY_SPACE,    .value = 1},
super_down       = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 1},
space_repeat     = {.type = EV_KEY, .code = KEY_SPACE,    .value = 2},
super_repeat     = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 2},
syn              = {.type = EV_SYN, .code = SYN_REPORT,   .value = 0};
// clang-format on

int equal(const struct input_event *first, const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
        exit(EXIT_FAILURE);
}

flags_t parse_flags(int argc, char* argv[]) {
    if (argc == 1) {
        return no_flags;
    }
    if (argc > 2) {
        return help_flag;
    }

    if (strcmp(argv[1], "-h") == 0) {
        return help_flag;
    }

    return help_flag;
}

void show_help() {
    fprintf(stdout,
        "For usage see https://gitlab.com/interception/linux/plugins/caps2space\n" \
        "\n" \
        "  FLAGS:\n" \
        "    -h  - show this help message\n" \
    );
}

int main(int argc, char* argv[]) {
    flags_t flags = parse_flags(argc, argv);
    if (flags & help_flag) {
        show_help();
        return 0;
    }

    int space_is_down = 0, space_give_up = 0;
    struct input_event input;

    setbuf(stdin, NULL), setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN)
            continue;

        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }

        if (space_is_down) {
            if (equal(&input, &space_down) ||
                equal(&input, &space_repeat))
                continue;

            if (equal(&input, &space_up)) {
                space_is_down = 0;
                if (space_give_up) {
                    space_give_up = 0;
                    write_event(&super_up);
                    continue;
                }
                write_event(&space_down);
                write_event(&syn);
                usleep(20000);
                write_event(&space_up);
                continue;
            }

            if (!space_give_up && input.value) {
                space_give_up = 1;
                write_event(&super_down);
                write_event(&syn);
                usleep(20000);
            }
        } else if (equal(&input, &space_down)) {
            space_is_down = 1;
            continue;
        }

        write_event(&input);
    }
}
